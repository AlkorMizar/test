package main

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestPingHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/ping", nil)
	if err != nil {
		t.Fatalf("Failed to create request: %v", err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(PingHandler)
	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusOK {
		t.Errorf("Expected status code %d, but got %d", http.StatusOK, rr.Code)
	}

	expectedResponseBody := "pong\nServer name: ADMIN\n"
	actualResponseBody, err := ioutil.ReadAll(rr.Body)
	if err != nil {
		t.Fatalf("Failed to read response body: %v", err)
	}

	if string(actualResponseBody) != expectedResponseBody {
		t.Errorf("Expected response body: %q\nBut got: %q", expectedResponseBody, actualResponseBody)
	}
}

func TestMain(m *testing.M) {
	retCode := m.Run()
	os.Exit(retCode)
}
