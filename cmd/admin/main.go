package main

import (
	"fmt"
	"net/http"
	"os"
)

func PingHandler(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "pong\n")
	fmt.Fprintf(w, "Server name: %s\n", os.Getenv("APP_NAME"))
}

func main() {
	http.HandleFunc("/ping", PingHandler)
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		fmt.Println("Error starting server:", err)
	}
}
