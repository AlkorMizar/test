package main

import (
	"fmt"
	"net/http"
	"os"
)

func pingHandler(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "pong\npong")
	fmt.Fprintf(w, "Server name: %s\n", os.Getenv("APP_NAME"))
}

func main() {
	http.HandleFunc("/ping", pingHandler)
	_ = http.ListenAndServe(":8080", nil)

}
